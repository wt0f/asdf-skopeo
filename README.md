# skopeo asdf Plugin

![Build Status](https://gitlab.com/wt0f/asdf-skopeo/badges/main/pipeline.svg)

This is the plugin repo for [asdf-vm/asdf](https://github.com/asdf-vm/asdf.git)
to manage [containers/skopeo](https://github.com/containers/skopeo).

## Install

After installing [asdf](https://github.com/asdf-vm/asdf),
you can add this plugin like this:

```bash
asdf plugin add skopeo
asdf install skopeo v1.7.0
asdf global skopeo v1.7.0
```
